<?php

namespace App\Http\Controllers;

use App\Category;
use App\Transaction;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class TransactionsController extends Controller
{
	public function index()
	{
		return view('transactions.index', [
			'transactions' => Transaction::all()
		]);
	}

	public function show(Transaction $transaction)
	{
		return view('transactions.show', compact('transaction'));	
	}

    public function create()
    {
        return view('transactions.create', ['categories' => Category::all()]);    
    }

    public function store()
    {
        $validated_inputs = $this->validate(request(), $this->validationRules());

    	Transaction::create($validated_inputs + ['user_id' => Auth::user()->id]);

    	return redirect()->route('transactions.index');
    }

    public function edit(Transaction $transaction)
    {
        $categories = Category::all();

        return view('transactions.edit', compact('transaction', 'categories'));
    }

    public function update(Request $request, Transaction $transaction)
    {
        $validated_inputs = $this->validate($request, $this->validationRules());

    	$transaction->fill($validated_inputs)->save();

    	return redirect()->route('transactions.index');
    }

    public function destroy($transaction)
    {
    	Transaction::destroy($transaction);

    	return redirect()->route('transactions.index');
    }

    protected function validationRules()
    {
        return [
            'category_id' => 'required|exists:categories,id',
            'amount' => 'required|numeric',
            'date' => 'required',
            'description' => 'required|max:255',
            'notes' => 'max:255',
            'place' => 'max:255',
        ];
    }
}
