<?php

namespace App\Http\Controllers\Api;

use App\Category;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class CategoriesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @todo  Paginate response
     * 
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return response()->json(Category::forAuthedUser()->get()->toArray());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|max:33'
        ]);

        $category = Category::create($request->all() + ['user_id' => Auth::user()->id]);

        return response()->json([
            'data' => ['message' => 'resource created successfully']
        ], 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $category = Category::forAuthedUser()->find($id);

        if (! $category) {
            return response()->json(['errors' => ["This resource instance doesn't exist"]], 404);
        }

        return response()->json(['data' => $category->toArray()]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'required|max:33'
        ]);
        
        $category = Category::forAuthedUser()->find($id);

        if (! $category) {
            return response()->json(['errors' => ["This resource instance doesn't exist"]], 404);
        }

        $category->fill($request->all())->save();

        return response()->json([
            'data' => $category->toArray(), 
            'message' => 'resource updated successfully'
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $category = Category::forAuthedUser()->find($id);

        if (! $category) {
             return response()->json(['errors' => ["This resource instance doesn't exist"]], 404);
        }

        $category->delete();

        return response()->json(['message' => 'resource destroyed successfully']);        
    }
}
