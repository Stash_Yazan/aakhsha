<?php

namespace App\Http\Controllers\Api;

use App\Transaction;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class TransactionsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // Placeholder
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        // Placeholder
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validated_inputs = $this->validate($request, $this->validationRules());

        Transaction::create($validated_inputs + ['user_id' => Auth::user()->id]);

        return response()->json([
            'data' => ['message' => 'resource created successfully']
        ], 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $transaction = Transaction::with('category')->find($id);

        if (! $transaction) {
            return response()->json(['errors' => ["This resource instance doesn't exist"]], 404);
        }

        return response()->json(['data' => $transaction->toArray()]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        // Placeholder
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validated_inputs = $this->validate($request, $this->validationRules());

        $transaction = Transaction::find($id);

        if (! $transaction) {
            return response()->json(['errors' => ["This resource instance doesn't exist"]], 404);
        }

        $transaction->fill($validated_inputs)->save();

        return response()->json([
            'data' => $transaction->toArray(), 
            'message' => 'resource updated successfully'
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $transaction = Transaction::find($id);

        if (! $transaction) {
             return response()->json(['errors' => ["This resource instance doesn't exist"]], 404);
        }

        $transaction->delete();

        return response()->json(['message' => 'resource destroyed successfully']);
    }

    protected function validationRules()
    {
        return [
            'category_id' => 'required|exists:categories,id',
            'amount' => 'required|numeric',
            'date' => 'required',
            'description' => 'required|max:255',
            'notes' => 'max:255',
            'place' => 'max:255',
        ];   
    }
}
