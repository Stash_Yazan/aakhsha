<?php

namespace App\Http\Controllers;

use App\Category;
use Illuminate\Http\Request;

class CategoriesController extends Controller
{
	public function index()
	{
		return view('categories.index', [
			'categories' => Category::ForAuthedUser()->get()
		]);
	}

	public function show($category)
	{  
		return view('categories.show', [
			'category' => Category::ForAuthedUser()->findOrFail($category)
		]);
	}

    public function store(Request $request)
    {
        $this->validate($request, [
            'user_id' => 'required|exists:users,id',
            'name' => 'required|max:33'
        ]);

    	Category::create($request->all());

    	return redirect()->route('categories.index');
    }

    public function update(Request $request, $category)
    {
        $this->validate($request, [
            'name' => 'required|max:33'
        ]);

        $category = Category::ForAuthedUser()->findOrFail($category);

    	$category->name = $request->name;
    	$category->save();

    	return redirect()->route('categories.index');
    }

    public function destroy($category)
    {
        Category::ForAuthedUser()->findOrFail($category)->delete();

    	return redirect()->route('categories.index');
    }
}
