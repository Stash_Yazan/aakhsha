<?php

namespace App;

use App\User;
use App\Category;
use Carbon\Carbon;
use App\TransactionAmount;
use App\Scopes\AuthedUserScope;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Exceptions\InvalidTransactionAmountException;

class Transaction extends Model
{
    use SoftDeletes;
    
    /** bypass mass-assignment protection */
    protected $guarded = [];

    /** attributes to be casted to Carbon instances */
    protected $dates = ['deleted_at'];

    /**
     * The "booting" method of the model.
     *
     * @return void
     */
    protected static function boot()
    {
        parent::boot();

        static::addGlobalScope(new AuthedUserScope);
    }

    /*
    |--------------------------------------------------------------------------
    | Relationships
    |--------------------------------------------------------------------------
    |
    | Here is where you can define model relationships.
    |
    */
    public function user()
    {
        return $this->belongsTo(User::class);    
    }

    public function category()
    {
        return $this->belongsTo(Category::class);
    }

    /*
    |--------------------------------------------------------------------------
    | Accessors And Mutators
    |--------------------------------------------------------------------------
    |
    | Here is where you can create attribute accessors and mutators.
    |
    */

    /**
     * Place a fake dot in the string to express a "float"
     */
    public function getAmountAttribute($amount)
    {
        return substr_replace($amount, '.', -2, 0);
    }

    /**
     * Convert the input to a float-string, 
     * and then remove the dot to save it as integer
     */
    public function setAmountAttribute($amount)
    {
        try {
            $floated_amount = number_format($amount, 2, '.', '');
        } catch (\Exception $e) {
            throw new InvalidTransactionAmountException();
        }

        $this->attributes['amount'] = str_replace('.', '', $floated_amount);
    }

    /**
     * Leverage Carbon's parse magic to easily normalize the input.
     */
    public function setDateAttribute($date)
    {
        $this->attributes['date'] = Carbon::parse($date)->format('Y-m-d H:i:s');
    }
}
