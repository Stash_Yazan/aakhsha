<?php

namespace App\Scopes;

use Exception;
use Illuminate\Database\Eloquent\Scope;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;

class AuthedUserScope implements Scope
{
    /**
     * Apply the scope to a given Eloquent query builder.
     *
     * @param  \Illuminate\Database\Eloquent\Builder  $builder
     * @param  \Illuminate\Database\Eloquent\Model  $model
     * @return void
     */
    public function apply(Builder $builder, Model $model)
    {
        if (! auth()->check()) {
            throw new Exception("Trying to apply a scope that requires an authed user, but no user was authed.", 1);
        }

        $builder->where('user_id', auth()->user()->id);
    }
}