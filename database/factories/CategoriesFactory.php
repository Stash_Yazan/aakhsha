<?php

use App\User;
use App\Category;
use Faker\Generator as Faker;

$factory->define(Category::class, function (Faker $faker) {
    return [
    	'user_id' => factory(User::class)->create()->id,
        'name' => $faker->sentence(2),
    ];
});
