<?php

use App\User;
use App\Category;
use Faker\Generator as Faker;

$factory->define(App\Transaction::class, function (Faker $faker) {
    return [
    	'user_id' => factory(User::class)->create()->id,
    	'category_id' => factory(Category::class)->create()->id,
        'amount' => '444.23',
        'date' => $faker->dateTime->format('Y-m-d H:i:s'),
        'description' => $faker->sentence,
        'notes' => $faker->sentence,
        'place' => $faker->word,
    ];
});
