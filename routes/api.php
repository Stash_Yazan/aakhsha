<?php

Route::middleware('auth:api')->prefix('v1')->namespace('Api')->as('api.')->group(function(){
	Route::resource('transactions', 'TransactionsController');
	Route::resource('categories', 'CategoriesController');
});

