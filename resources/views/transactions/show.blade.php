@extends('layouts.app')

@section('content')
			  
  <div class="md:flex">
    <div class="w-1/2 m-3 mb-6">
      <h3 class="text-base text-grey-darker py-2 border-b-2">Date</h3>
      <p class="text-lg pt-3">{{ $transaction->date }}</p>
    </div>
    
    <div class="w-1/2 m-3 mb-6">
      <h3 class="text-base text-grey-darker py-2 border-b-2">Category</h3>
      <p class="text-lg pt-3">{{ $transaction->category->name }}</p>
    </div>
  </div>
  
  <div class="md:flex">
    <div class="m-3 mb-6 flex-auto">
      <h3 class="text-base text-grey-darker py-2 border-b-2">Description</h3>
      <p class="text-lg pt-3">{{ $transaction->description }}</p>
    </div>
  </div>
  
  <div class="md:flex">
    <div class="m-3 mb-6 flex-auto">
      <h3 class="text-base text-grey-darker py-2 border-b-2">Notes</h3>
      <p class="text-lg pt-3">{{ $transaction->notes }}</p>
    </div>
  </div>
  
  <div class="md:flex">
    <div class="m-3 mb-6 flex-auto">
      <h3 class="text-base text-grey-darker py-2 border-b-2">Place</h3>
      <p class="text-lg pt-3">{{ $transaction->place }}</p>
    </div>
  </div>
  
  <div class="md:flex">
    <div class="m-3 mb-6 flex-auto">
      <h3 class="text-base text-grey-darker py-2 border-b-2">Amount</h3>
      <p class="text-lg pt-3">{{ $transaction->amount }} JD</p>
    </div>
  </div>
  
  <div class="md:flex">
    <div class="m-3 mb-6 mt-5">
      <a href="{{ route('transactions.edit', [$transaction->id]) }}" class="btn btn-blue no-underline">Edit</a>
    </div>

    <div class="m-3 mb-6 mt-1">
    	<form method="post" action="{{ route('transactions.destroy', [$transaction->id]) }}">
    		{{ csrf_field() }}
    		{{ method_field('delete') }}
	    	<input type="submit" class="btn btn-red no-underline cursor-pointer" value="Delete">
    	</form>
    </div>
  </div>

@endsection