<form method="post" action="{{ ($edit ?? false) ? route('transactions.update', [$transaction->id]) : route('transactions.store') }}">
		
  {{ csrf_field() }}
  
  @if ($edit ?? false)
    {{ method_field('patch') }}
  @endif

  <div class="md:flex">
    <div class="w-1/2 m-3" id="datepicker">
      <label class="label">Date</label>
        <date-picker name="date" value="{{ $transaction->date ?? date('m/d/Y') }}" input-class="text-field hover:border-blue border-blue-lighter"></date-picker>
    </div>
    
    <div class="w-1/2 m-3">
      <label class="label">Category</label>
      <select name="category_id" class="text-field hover:border-blue border-blue-lighter">
        
        @foreach($categories as $category)
          <option value="{{ $category->id }}">{{ $category->name }}</option>
        @endforeach
        
      </select>
    </div>
  </div>
  
  <div class="md:flex">
    <div class="m-3 flex-auto">
      <label class="label">Description</label>
      <input type="text" name="description" class="text-field hover:border-blue border-blue-lighter" value="{{ $transaction->description ?? '' }}">
    </div>
  </div>
  
  <div class="md:flex">
    <div class="m-3 flex-auto">
      <label class="label">Notes</label>
      <textarea type="text" name="notes" rows="3" class="text-field hover:border-blue border-blue-lighter">{{ $transaction->notes ?? '' }}</textarea>
    </div>
  </div>
  
  <div class="md:flex">
    <div class="m-3 flex-auto">
      <label class="label">Place</label>
      <input type="text" name="place" class="text-field hover:border-blue border-blue-lighter" value="{{ $transaction->place ?? '' }}">
    </div>
  </div>
  
  <div class="md:flex">
    <div class="m-3 flex-auto">
      <label class="label">Amount</label>
      <input type="text" name="amount" class="text-field hover:border-blue border-blue-lighter" value="{{ $transaction->amount ?? '' }}">
    </div>
  </div>
  
  <div class="md:flex">
    <div class="m-3 flex-auto">
      <button type="submit" class="btn btn-blue">Save</button>
    </div>
  </div>
  
</form>

<script>
  new Vue({
    el: '#datepicker'
  });
</script>