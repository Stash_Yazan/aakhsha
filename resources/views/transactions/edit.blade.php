@extends('layouts.app')

@section('content')

  @include('transactions.form', ['edit' => true])

@endsection