@extends('layouts.app')

@section('content')
	
	<div class="overflow-x-scroll">
		<table>

			<tr class="w-full">
				<th class="w-1/6 border-b-2 p-2 border-blue-lighter">Date</th>
				<th class="w-3/6 border-b-2 p-2 border-blue-lighter">Description</th>
				<th class="w-1/6 border-b-2 p-2 border-blue-lighter">Category</th>
				<th class="w-1/6 border-b-2 p-2 border-blue-lighter">Amount</th>
				<th class="w-1/6 bg-grey-lighter"></th>
			</tr>
			
			@foreach($transactions as $expense)	
			<tr class="w-full">
				<td class="w-1/6 p-2 text-center">{{ $expense->date }}</td>
				<td class="w-3/6 p-2 text-center">{{ $expense->description }}</td>
				<td class="w-1/6 p-2 text-center">{{ $expense->category->name }}</td>
				<td class="w-1/6 p-2 text-center">{{ $expense->amount }} JD</td>
				<td class="w-1/6 p-2 text-center flex justify-between">

					<form method="GET" action="{{ route('transactions.edit', [$expense->id]) }}">
						<button class="btn text-grey-dark hover:bg-blue-lighter mx-2">E</button>
					</form>

					<form method="POST" action="{{ route('transactions.destroy', [$expense->id]) }}">
						{{ csrf_field() }}
						{{ method_field('delete') }}
						<button type="submit" class="btn text-grey-dark hover:bg-red-lighter mx-2">D</button>
					</form>
				</td>
			</tr>
			@endforeach

		</table>		
	</div>

	{{-- {{ $transactions->links() }} --}}
@endsection