<nav id="navbar" class="flex items-center justify-between flex-wrap bg-blue p-6 rounded">

    <div class="flex items-center flex-no-shrink text-white mr-6">
        <span class="text-xl">ExMan Pro</span>
    </div>

    <div class="block lg:hidden">
        <button @click="toggleMenu" class="flex items-center px-3 py-2 border rounded text-blue-lighter border-blue-light hover:text-white hover:border-white">
            <svg class="fill-current h-3 w-3" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg"><title>Menu</title><path d="M0 3h20v2H0V3zm0 6h20v2H0V9zm0 6h20v2H0v-2z"/></svg>
        </button>
    </div>

    <div :class="['w-full block flex-grow lg:flex lg:items-center lg:w-auto', {'xs:hidden' : ! menuVisible}]">
        <div class="text-sm lg:flex-grow">
            <!-- Authentication Links -->
            @guest
                <a href="{{ route('login') }}" class="block mt-4 lg:inline-block lg:mt-0 text-blue-lighter hover:text-white mr-4">Login</a>
                <a href="{{ route('register') }}" class="block mt-4 lg:inline-block lg:mt-0 text-blue-lighter hover:text-white mr-4">Register</a>
            @else

            <div>
                <a href="{{ route('transactions.index') }}" class="block mt-4 lg:inline-block lg:mt-0 text-blue-lighter hover:text-white mr-4 no-underline">Expenses</a>
                <a href="{{ route('transactions.create') }}" class="block mt-4 lg:inline-block lg:mt-0 text-blue-lighter hover:text-white mr-4 no-underline">Add Expenses</a>

                {{-- <a href="{{ route('categories.index') }}" class="block mt-4 lg:inline-block lg:mt-0 text-blue-lighter hover:text-white mr-4 no-underline">Categories</a> --}}
                {{-- <a href="{{ route('categories.create') }}" class="block mt-4 lg:inline-block lg:mt-0 text-blue-lighter hover:text-white mr-4 no-underline">Add Category</a> --}}
            </div>
        </div>
        <div>
           <a href="{{ route('logout') }}" class="inline-block text-sm px-4 py-2 leading-none border rounded text-white border-white hover:border-transparent hover:text-blue hover:bg-white mt-4 lg:mt-0" 
                    onclick="event.preventDefault();
                    document.getElementById('logout-form').submit();">
                    Logout
                </a>

            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                {{ csrf_field() }}
            </form> 
        </div>
            @endguest

</div>
</nav>

<script>
    new Vue({
        el: '#navbar',

        data: {
            menuVisible: false
        },

        methods: {
            toggleMenu() {
                if (this.menuVisible)
                {
                    this.menuVisible = false;
                } else {
                    this.menuVisible = true;
                }
            }
        }
    });
</script>