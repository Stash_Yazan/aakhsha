@extends('layouts.app')

@section('content')
			  
  <div class="md:flex">
    <div class="w-1/2 m-3 mb-6">
      <h3 class="text-base text-grey-darker py-2 border-b-2">Category Name</h3>
      <p class="text-lg pt-3">{{ $category->name }}</p>
    </div>
  
  {{-- <div class="md:flex">
    <div class="m-3 mb-6 mt-5">
      <a href="{{ route('transactions.edit', [$transaction->id]) }}" class="btn btn-blue no-underline">Edit</a>
    </div>

    <div class="m-3 mb-6 mt-1">
    	<form method="post" action="{{ route('transactions.destroy', [$transaction->id]) }}">
    		{{ csrf_field() }}
    		{{ method_field('delete') }}
	    	<input type="submit" class="btn btn-red no-underline cursor-pointer" value="Delete">
    	</form>
    </div> --}}
  </div>

@endsection