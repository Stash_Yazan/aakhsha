<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Styles -->
    <link href="{{ mix('css/app.css') }}" rel="stylesheet">

    <script src="{{ mix('js/app.js') }}"></script>

</head>
<body>
    <div id="app">
        @include ('partials.nav')
        

        <div class="bg-grey-lighter text-grey-dark h-screen pt-8">
            <div class="px-6 pb-8 pt-20 md:pt-16 w-full max-w-lg mx-auto pt-8 shadow">
                
                <div>
                    @component('partials.errors') @endcomponent
                    @component('partials.success') @endcomponent
                </div>
                
                @yield('content')
            </div>
        </div>
    </div>

    @yield('scripts')
</body>
</html>
