<?php

namespace Tests;

use App\User;
use Exception;
use App\Exceptions\Handler;
use Laravel\Passport\Passport;
use Illuminate\Contracts\Debug\ExceptionHandler;
use Illuminate\Foundation\Testing\TestCase as BaseTestCase;

abstract class TestCase extends BaseTestCase
{
    use CreatesApplication;

    protected $auth_type = 'web';

    protected function disableExceptionHandling()
    {
        $this->app->instance(ExceptionHandler::class, new class extends Handler {
            public function __construct() {}
            public function report(Exception $e) {}
            public function render($request, Exception $exception) {
                throw $exception;
    		}
    	});
    }

    protected function login($user = null, $scopes = [])
    {
        if (is_null($user)) {
            $user = factory(User::class)->create();
        }

        if ($this->auth_type == 'web') {
            return $this->actingAs($user);
        }

        return Passport::actingAs($user, $scopes);
    }

    protected function apiAuth()
    {
        $this->auth_type = 'api';

        return $this;
    }
}
