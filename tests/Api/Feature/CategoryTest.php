<?php

namespace Tests\Feature\Api\Feature;

use App\User;
use App\Category;
use Tests\TestCase;
use App\Transaction;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class CategoryTest extends TestCase
{
	use RefreshDatabase;

    /**
     * NOTE: A "Customer" is an authenticated user, where is a 
     * "User" is a generic person who uses the application.
     */
    
    /** @test */
    public function a_customer_can_create_a_category()
    {   
        $this->apiAuth()->login();

        $category = factory(Category::class)->make();

        $response = $this->json('POST', '/api/v1/categories', $category->toArray());

        $response->assertStatus(201);
        $response->assertJsonFragment(['message' => 'resource created successfully']);
    }

    /** @test */
    public function it_validates_requests_on_creation()
    {
        $this->apiAuth()->login();

        // Name longer than 33 characters
        $category = factory(Category::class)->make(['name' => 'invalid category name because its longer than 33 characters']);
        $response = $this->json('POST', '/api/v1/categories', $category->toArray());
        $response->assertStatus(422);
        $response->assertJsonValidationErrors('name');

        // Name is missing
        $response = $this->json('POST', '/api/v1/categories', []);
        $response->assertStatus(422);
        $response->assertJsonValidationErrors('name');
    }

    /** @test */
    public function a_guest_cannot_create_a_category()
    {
        $category = factory(Category::class)->make();

        $response = $this->json('POST', '/api/v1/categories', $category->toArray());

        $response->assertStatus(401);
        $response->assertJsonFragment(['message' => 'Unauthenticated.']);
    }

    /** @test */
    public function a_customer_can_view_a_listing_of_his_categories()
    {
        $user = factory(User::class)->create();
        $categories = factory(Category::class, 3)->create(['user_id' => $user->id]);

        $this->apiAuth()->login($user);

        $response = $this->json('GET', '/api/v1/categories');

        $categories->each(function($category) use ($response){
            $response->assertJsonFragment(['name' => $category->name]);
        });
    }

    /** @test */
    public function a_customer_can_only_see_his_categories_list()
    {
        $authed_user = factory(User::class)->create();
        $owning_user = factory(User::class)->create();

        $categories = factory(Category::class, 3)->create(['user_id' => $owning_user->id]);

        $this->apiAuth()->login($authed_user);

        $response = $this->json('GET', '/api/v1/categories');

        $categories->each(function($category) use ($response){
            $response->assertJsonMissing(['name' => $category->name]);            
        });
    }

    /** @test */
    public function a_customer_can_view_a_single_category()
    {   
        $user = factory(User::class)->create();
        
        $this->apiAuth()->login($user);

        $category = factory(Category::class)->create(['user_id' => $user->id]);

        $response = $this->json('GET', '/api/v1/categories/' . $category->id);

        $response->assertStatus(200)
                 ->assertJson(['data' => $category->toArray()]);
    }

    /** @test */
    public function a_customer_cannot_view_a_category_that_is_not_his()
    {   
        $user = factory(User::class)->create();

        $this->apiAuth()->login($user);

        $unViewableCategory = factory(Category::class)->create();

        $response = $this->json('GET', '/api/v1/categories/' . $unViewableCategory->id);

        $response->assertStatus(404)
                 ->assertJsonMissing(['data' => $unViewableCategory->toArray()]);
    }

    /** @test */
    public function a_customer_can_update_a_category()
    {
        $user = factory(User::class)->create();

        $this->apiAuth()->login($user);

        $category = factory(Category::class)->create(['user_id' => $user->id]);

        $category->name = 'New Category Name';

        $response = $this->json('PUT', '/api/v1/categories/' . $category->id, $category->toArray());

        $response->assertJsonFragment(['message' => 'resource updated successfully']);
        $response->assertJsonFragment(['name' => 'New Category Name']);
    }

    /** @test */
    public function it_validates_requests_on_update()
    {
        $this->apiAuth()->login();

        $category = factory(Category::class)->create();

        // Name longer than 33 characters
        $category->name = 'invalid category name because its longer than 33 characters';
        $response = $this->json('PATCH', "/api/v1/categories/{$category->id}", $category->toArray());
        $response->assertStatus(422);
        $response->assertJsonValidationErrors('name');

        // Name is missing
        $response = $this->json('PATCH', "/api/v1/categories/{$category->id}", []);
        $response->assertStatus(422);
        $response->assertJsonValidationErrors('name');
    }

    /** @test */
    public function a_customer_cannot_update_a_category_that_is_not_his()
    {
        $user = factory(User::class)->create();
        $another_user = factory(User::class)->create();
        
        $this->apiAuth()->login($user);

        $category = factory(Category::class)->create(['user_id' => $another_user->id]);

        $category->name = 'New Category Name';

        $response = $this->json('PUT', '/api/v1/categories/' . $category->id, $category->toArray());

        $response->assertStatus(404);
    }

    /** @test */
    public function a_customer_can_delete_a_category()
    {
        $user = factory(User::class)->create();

        $this->apiAuth()->login($user);

        $category = factory(Category::class)->create(['user_id' => $user->id]);

        $response = $this->json('DELETE', '/api/v1/categories/' . $category->id);

        $response->assertJsonFragment(['message' => 'resource destroyed successfully']);
    }

    /** @test */
    public function a_customer_cannot_delete_a_category_that_is_not_his()
    {
        $user = factory(User::class)->create();
        $another_user = factory(User::class)->create();

        $this->apiAuth()->login($user);

        $category = factory(Category::class)->create(['user_id' => $another_user->id]);

        $response = $this->json('DELETE', '/api/v1/categories/' . $category->id);

        $response->assertStatus(404);
    }
}
