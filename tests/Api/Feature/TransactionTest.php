<?php

namespace Tests\Feature\Api\Feature;

use App\User;
use Tests\TestCase;
use App\Transaction;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class TransactionTest extends TestCase
{
	use RefreshDatabase;

    /**
     * @test
     */
    public function a_customer_can_record_a_transaction()
    {
        $user = factory(User::class)->create();
        $transaction = factory(Transaction::class)->make(['user_id' => $user->id]);

    	$this->apiAuth()->login($user);

		$response = $this->json('POST', '/api/v1/transactions', $transaction->toArray());

		$response->assertStatus(201);
    		$response->assertJsonFragment(['message' => 'resource created successfully']);
    }

    /** @test */
    public function it_validates_requests_on_creation()
    {
        $this->apiAuth()->login();

        // category_id, amount, date, and description fields are missing
        $transaction = factory(Transaction::class)->make();
        unset($transaction->category_id, $transaction->amount, $transaction->date, $transaction->description);
        $response = $this->json('POST', '/api/v1/transactions', $transaction->toArray());
        $response->assertStatus(422);
        $response->assertJsonValidationErrors(['category_id', 'amount', 'date', 'description']);

        // Invalid category id
        $transaction = factory(Transaction::class)->make(['category_id' => 99]);
        $response = $this->json('POST', '/api/v1/transactions', $transaction->toArray());
        $response->assertStatus(422);
        $response->assertJsonValidationErrors('category_id');

        // amount is not numeric
        $transaction = factory(Transaction::class)->make();
        $response = $this->json('POST', '/api/v1/transactions', array_merge(
            $transaction->toArray(), 
            ['amount' => 'non-numeric value'])
        );  
        $response->assertStatus(422);
        $response->assertJsonValidationErrors('amount');


        // description, notes and place fields are longer than 255
        $transaction = factory(Transaction::class)->make([
            'description' => str_random(256),
            'notes' => str_random(256),
            'place' => str_random(256),
        ]);
        $response = $this->json('POST', '/api/v1/transactions', $transaction->toArray());
        $response->assertStatus(422);
        $response->assertJsonValidationErrors(['description', 'notes', 'place']);
    }

    /** @test */
    public function a_guest_cannot_create_a_transaction()
    {
        $transaction = factory(Transaction::class)->make();

        $response = $this->json('POST', 'api/v1/transactions', $transaction->toArray());

        $response->assertStatus(401);
        $response->assertJsonFragment(['message' => 'Unauthenticated.']);
    }

    /**
     * @test
     */
    public function a_customer_can_only_see_his_transactions()
    {
        $user = factory(User::class)->create();
        $viewableTransaction = factory(Transaction::class)->create(['user_id' => $user->id]);
        $unViewableTransaction = factory(Transaction::class)->create();

        $this->apiAuth()->login($user);

     	$response = $this->json('GET', 'api/v1/transactions/' . $viewableTransaction->id);

    	$response->assertJsonFragment(
			['description' => $viewableTransaction->description]
    	);

    	$response->assertStatus(200);

    	$response = $this->json('GET', 'api/v1/transactions/' . $unViewableTransaction->id);

    	$response->assertStatus(404);
    }

    /**
     * @test
     */
    public function a_customer_can_view_a_single_transaction()
    {    	
        $user = factory(User::class)->create();
        $transaction = factory(Transaction::class)->create(['user_id' => $user->id]);

    	$this->apiAuth()->login($user);

        $response = $this->json('GET', 'api/v1/transactions/' . $transaction->id);

        $response->assertJsonFragment(['amount' => $transaction->amount]);
        $response->assertJsonFragment(['date' => $transaction->date]);
        $response->assertJsonFragment(['name' => $transaction->category->name]);
        $response->assertJsonFragment(['description' => $transaction->description]);
		$response->assertJsonFragment(['notes' => $transaction->notes]);
        $response->assertJsonFragment(['place' => $transaction->place]);
    }

    /**
     * @test
     */
    public function a_customer_can_update_a_transaction()
    {
        $user = factory(User::class)->create();
        $transaction = factory(Transaction::class)->create(['user_id' => $user->id]);

    	$this->apiAuth()->login($user);

        $transaction->amount = '444.44';
        $transaction->date = '2018-08-08 08:08:08';
        $transaction->description = 'transaction description';
        $transaction->notes = 'transaction note';
        $transaction->place = 'transaction place';
            
        $response = $this->json('PUT', 'api/v1/transactions/' . $transaction->id, $transaction->toArray());

        $updatedTransaction = $transaction->fresh();

        $response->assertJsonFragment(['message' => 'resource updated successfully']);
        $response->assertJsonFragment(['amount' => '444.44']);
        $response->assertJsonFragment(['date' => '2018-08-08 08:08:08']);
        $response->assertJsonFragment(['description' => 'transaction description']);
		$response->assertJsonFragment(['notes' => 'transaction note']);
        $response->assertJsonFragment(['place' => 'transaction place']);
    }

    /** @test */
    public function it_validates_requests_on_update()
    {
        $this->apiAuth()->login($user = factory(User::class)->create());

        // category_id, amount, date, and description fields are missing
        $transaction = factory(Transaction::class)->create(['user_id' => $user->id]);
        unset($transaction->category_id, $transaction->amount, $transaction->date, $transaction->description);
        $response = $this->json('PATCH', "/api/v1/transactions/{$transaction->id}", $transaction->toArray());
        $response->assertStatus(422);
        $response->assertJsonValidationErrors(['category_id', 'amount', 'date', 'description']);

        // Invalid category id
        $transaction = factory(Transaction::class)->create(['user_id' => $user->id]);
        $response = $this->json('PATCH', "/api/v1/transactions/{$transaction->id}", array_merge(
            $transaction->toArray(), 
            ['category_id' => 99]
        ));
        $response->assertStatus(422);
        $response->assertJsonValidationErrors('category_id');

        // amount is not numeric
        $transaction = factory(Transaction::class)->create(['user_id' => $user->id]);
        $response = $this->json('PATCH', "/api/v1/transactions/{$transaction->id}", array_merge(
            $transaction->toArray(), 
            ['amount' => 'non-numeric value'])
        );  
        $response->assertStatus(422);
        $response->assertJsonValidationErrors('amount');

        // description, notes and place fields are longer than 255
        $transaction = factory(Transaction::class)->create(['user_id' => $user->id]);
        unset($transaction->id);
        $response = $this->json('PATCH', "/api/v1/transactions/{$user->id}", array_merge(
            $transaction->toArray(),
            [
                'description' => str_random(256),
                'notes' => str_random(256),
                'place' => str_random(256),
            ]
        ));
        $response->assertStatus(422);
        $response->assertJsonValidationErrors(['description', 'notes', 'place']);
    }

   /**
    * @test
    */
   public function a_customer_can_delete_a_transaction()
   {
   		$user = factory(User::class)->create();
       	$transaction = factory(Transaction::class)->create(['user_id' => $user->id]);

   		$this->apiAuth()->login($user);

       	$response = $this->delete('/api/v1/transactions/' . $transaction->id);

       	$response->assertJsonFragment(['message' => 'resource destroyed successfully']);
   }
}
