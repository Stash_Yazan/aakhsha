<?php

namespace Tests\Web\Feature;

use App\User;
use App\Category;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class CategoryTest extends TestCase
{
	use RefreshDatabase;

	/**
	 * NOTE: A "Customer" is an authenticated user, where is a 
	 * "User" is a generic person who uses the application.
	 */
	
	/** @test */
	public function an_customer_can_create_a_category()
	{	
		$this->login();

	    $category = factory(Category::class)->make();

	    $response = $this->post('categories', $category->toArray());

	    $this->assertCount(1, Category::all());
	}

	/** @test */
	public function it_validates_the_request_on_creation()
	{
	    $this->login();

	    // Using an non-existing user
	    $category = factory(Category::class)->make(['user_id' => 99]);
	    $response = $this->post('categories', $category->toArray());
	    $response->assertSessionHasErrors('user_id');
	    $response->assertStatus(302);

	    // Name is missing
	    $response = $this->post('categories', ['name' => 'Category Name Category']);
	    $response->assertSessionHasErrors('user_id');
	    $response->assertStatus(302);

	    // Name is longer than 33 characters
	    $category = factory(Category::class)->make(['name' => 'invalid category name because its longer than 33.1 characters']);
	    $response = $this->post('categories', $category->toArray());
	    $response->assertSessionHasErrors('name');
	    $response->assertStatus(302);

	    // Name is missing
	    $response = $this->post('categories', ['user_id' => 1]);
	    $response->assertSessionHasErrors('name');
	    $response->assertStatus(302);

	}

	/** @test */
	public function unauthenticated_users_cannot_create_a_category()
	{
	    $category = factory(Category::class)->make();

	    $response = $this->post('categories', $category->toArray());

	    $this->assertCount(0, Category::all());
	}

	/** @test */
	public function an_customer_can_view_a_listing_of_his_categories()
	{
		$user = factory(User::class)->create();
	    $categories = factory(Category::class, 3)->create(['user_id' => $user->id]);

	    $this->login($user);

	    $response = $this->get('categories');

	    $response->assertSee($categories->first()->name);
	}

	/** @test */
	public function a_customer_can_only_see_his_categories_list()
	{
		$authed_user = factory(User::class)->create();
		$owning_user = factory(User::class)->create();

	    $categories = factory(Category::class, 3)->create(['user_id' => $owning_user->id]);

	    $this->login($authed_user);

	    $response = $this->get('categories');

	    $response->assertDontSee($categories->first()->name);
	}

	/** @test */
	public function a_customer_can_view_a_single_category()
	{	
		$user = factory(User::class)->create();
		$this->login($user);

		$category = factory(Category::class)->create(['user_id' => $user->id]);

		$response = $this->get('categories/' . $category->id);

		$response->assertStatus(200)
				 ->assertSee($category->name);
	}

	/** @test */
	public function a_customer_cannot_view_a_category_that_is_not_his()
	{	
		$user = factory(User::class)->create();
		$this->login($user);

		$unViewableCategory = factory(Category::class)->create();

		$response = $this->get('categories/' . $unViewableCategory->id);

		$response->assertStatus(404)
				 ->assertDontSee($unViewableCategory->name);
	}

	/** @test */
	public function a_customer_can_update_a_category()
	{
		$user = factory(User::class)->create();
		$this->login($user);

		$category = factory(Category::class)->create(['user_id' => $user->id]);

		$category->name = 'New Category Name';

		$response = $this->put('categories/' . $category->id, $category->toArray());

		$this->assertSame('New Category Name', Category::first()->name);
	}

	/** @test */
	public function it_validates_the_request_on_update()
	{
	    $this->login(
	    	$user = factory(User::class)->create()
	    );
	    
	    $category = factory(Category::class)->create(['user_id' => $user->id]);

	    // Name is longer than 33 characters
	    $category->name = 'invalid category name because its longer than 33.1 characters';
	    $response = $this->patch("categories/{$category->id}", $category->toArray());
	    $response->assertSessionHasErrors('name');
	    $response->assertStatus(302);

	    // Name is missing
	    $response = $this->patch("categories/{$category->id}", []);
	    $response->assertSessionHasErrors('name');
	    $response->assertStatus(302);
	}

	/** @test */
	public function a_customer_cannot_update_a_category_that_is_not_his()
	{
		$user = factory(User::class)->create();
		$another_user = factory(User::class)->create();
		$this->login($user);

		$category = factory(Category::class)->create(['user_id' => $another_user->id]);

		$category->name = 'New Category Name';

		$response = $this->put('categories/' . $category->id, $category->toArray());

		$response->assertStatus(404);
	}

	/** @test */
	public function a_customer_can_delete_a_category()
	{
		$user = factory(User::class)->create();
		$this->login($user);

	    $category = factory(Category::class)->create(['user_id' => $user->id]);

	    $this->delete('categories/' . $category->id);

	    $this->assertCount(0, Category::all());
	}

	/** @test */
	public function a_customer_cannot_delete_a_category_that_is_not_his()
	{
		$user = factory(User::class)->create();
		$another_user = factory(User::class)->create();
		$this->login($user);

	    $category = factory(Category::class)->create(['user_id' => $another_user->id]);

	    $response = $this->delete('categories/' . $category->id);

	    $response->assertStatus(404);
	}
}
