<?php

namespace Tests\Web\Feature;

use App\User;
use Tests\TestCase;
use App\Transaction;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class TransactionTest extends TestCase
{
	use RefreshDatabase;

    /**
     * NOTE: A "Customer" is an authenticated user, where is a 
     * "User" is a generic person who uses the application.
     */
    
	public function setUp()
	{
		parent::setUp();

    	// $this->disableExceptionHandling();
	}

    /**
     * @test
     */
    public function a_customer_can_record_a_transaction()
    {
        $user = factory(User::class)->create();
        $transaction = factory(Transaction::class)->make(['user_id' => $user->id]);

    	$this->login($user);

        $this->post('/transactions', $transaction->toArray());

		$response = $this->get('/transactions');

		$response->assertStatus(200);
        $response->assertSee($transaction->amount);
        $response->assertSee($transaction->date);
        $response->assertSee($transaction->category->name);
        $response->assertSee($transaction->description);
    }

    /** @test */
    public function it_validates_requests_on_creation()
    {
        $this->login();

        // category_id, amount, date, and description fields are missing
        $transaction = factory(Transaction::class)->make();
        unset($transaction->category_id, $transaction->amount, $transaction->date, $transaction->description);
        $response = $this->post('/transactions', $transaction->toArray());
        $response->assertStatus(302);
        $response->assertSessionHasErrors(['category_id', 'amount', 'date', 'description']);

        // Invalid category id
        $transaction = factory(Transaction::class)->make(['category_id' => 99]);
        $response = $this->post('/transactions', $transaction->toArray());
        $response->assertStatus(302);
        $response->assertSessionHasErrors('category_id');

        // amount is not numeric
        $transaction = factory(Transaction::class)->make();
        $response = $this->post('/transactions', array_merge(
            $transaction->toArray(), 
            ['amount' => 'non-numeric value'])
        );  
        $response->assertStatus(302);
        $response->assertSessionHasErrors('amount');


        // description, notes and place fields are longer than 255
        $transaction = factory(Transaction::class)->make([
            'description' => str_random(256),
            'notes' => str_random(256),
            'place' => str_random(256),
        ]);
        $response = $this->post('/transactions', $transaction->toArray());
        $response->assertStatus(302);
        $response->assertSessionHasErrors(['description', 'notes', 'place']);
    }

    /** @test */
    public function it_validates_requests_on_update()
    {
        $this->login($user = factory(User::class)->create());

        // category_id, amount, date, and description fields are missing
        $transaction = factory(Transaction::class)->create(['user_id' => $user->id]);
        unset($transaction->category_id, $transaction->amount, $transaction->date, $transaction->description);
        $response = $this->patch("/transactions/{$transaction->id}", $transaction->toArray());
        $response->assertStatus(302);
        $response->assertSessionHasErrors(['category_id', 'amount', 'date', 'description']);

        // Invalid category id
        $transaction = factory(Transaction::class)->create(['user_id' => $user->id]);
        $response = $this->patch("/transactions/{$transaction->id}", array_merge($transaction->toArray(), ['category_id' => 99]));
        $response->assertStatus(302);
        $response->assertSessionHasErrors('category_id');

        // amount is not numeric
        $transaction = factory(Transaction::class)->create(['user_id' => $user->id]);
        $response = $this->patch("/transactions/{$transaction->id}", array_merge(
            $transaction->toArray(), 
            ['amount' => 'non-numeric value'])
        );  
        $response->assertStatus(302);
        $response->assertSessionHasErrors('amount');

        // description, notes and place fields are longer than 255
        $transaction = factory(Transaction::class)->create(['user_id' => $user->id]);
        unset($transaction->id);
        $response = $this->patch("/transactions/{$user->id}", array_merge(
            $transaction->toArray(),
            [
                'description' => str_random(256),
                'notes' => str_random(256),
                'place' => str_random(256),
            ]
        ));
        $response->assertStatus(302);
        $response->assertSessionHasErrors(['description', 'notes', 'place']);
    }

    /** @test */
    public function a_guest_cannot_create_a_transaction()
    {
        $transaction = factory(Transaction::class)->make();

        $response = $this->post('transactions', $transaction->toArray());

        $response->assertRedirect('/login');
    }

    /**
     * @test
     */
    public function a_customer_can_only_see_his_transactions()
    {
        $user = factory(User::class)->create();
        $viewableTransaction = factory(Transaction::class)->create(['user_id' => $user->id]);
        $unViewableTransaction = factory(Transaction::class)->create();

        $this->login($user);

     	$response = $this->get('transactions/' . $viewableTransaction->id);
    	$response->assertSee($viewableTransaction->description);
    	$response->assertStatus(200);

    	$response = $this->get('transactions/' . $unViewableTransaction->id);
    	$response->assertStatus(404);
    }

    /**
     * @test
     */
    public function a_customer_can_view_a_single_transaction()
    {    	
        $user = factory(User::class)->create();
        $transaction = factory(Transaction::class)->create(['user_id' => $user->id]);

    	$this->login($user);

        $response = $this->get('/transactions/' . $transaction->id);

        $response->assertSee($transaction->amount);
        $response->assertSee($transaction->date);
        $response->assertSee($transaction->category->name);
        $response->assertSee($transaction->description);
		$response->assertSee($transaction->notes);
        $response->assertSee($transaction->place);
    }

    /**
     * @test
     */
    public function a_customer_can_update_a_transaction()
    {
        $user = factory(User::class)->create();
        $transaction = factory(Transaction::class)->create(['user_id' => $user->id]);

    	$this->login($user);

        $transaction->amount = '444.44';
        $transaction->date = '2018-08-08 08:08:08';
        $transaction->description = 'transaction description';
        $transaction->notes = 'transaction note';
        $transaction->place = 'transaction place';
            
        $this->put('transactions/' . $transaction->id, $transaction->toArray());

        $updatedTransaction = $transaction->fresh();

        $this->assertSame('444.44', $updatedTransaction->amount);
        $this->assertSame('2018-08-08 08:08:08', $updatedTransaction->date);
        $this->assertSame('transaction description', $updatedTransaction->description);
		$this->assertSame('transaction note', $updatedTransaction->notes);
        $this->assertSame('transaction place', $updatedTransaction->place);
    }

   /**
    * @test
    */
   public function a_customer_can_delete_a_transaction()
   {
   		$this->login();

       	$transaction = factory(Transaction::class)->create();

       	$response = $this->delete('transactions/' . $transaction->id);

       	$this->assertCount(0, Transaction::all());
   }
}
