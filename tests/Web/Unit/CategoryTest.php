<?php

namespace Tests\Web\Unit;

use App\User;
use App\Category;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class CategoryTest extends TestCase
{
	use RefreshDatabase;

	/** @test */
	public function a_category_belongs_to_a_user()
	{
		$user = factory(User::class)->create();
	    $category = factory(Category::class)->create(['user_id' => $user->id]);

	    $this->assertEquals($user->id, Category::first()->user->id);
	}
}
