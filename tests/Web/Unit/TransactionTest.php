<?php

namespace Tests\Web\Unit;

use App\User;
use App\Category;
use Tests\TestCase;
use App\Transaction;
use App\Scopes\AuthedUserScope;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use App\Exceptions\InvalidTransactionAmountException;

class TransactionTest extends TestCase
{
	use RefreshDatabase;

	/**
	 * @test
	 */
	public function a_transaction_is_associated_with_a_user()
	{
		$user = factory(User::class)->create();
		$transaction = factory(Transaction::class)->create(['user_id' => $user->id]);
		
	    $this->assertSame($transaction->user->id, $user->id);
	}

	/**
	 * @test
	 */
	public function a_transaction_belongs_to_a_category()
	{
		$category = factory(Category::class)->create();
		$transaction = factory(Transaction::class)->create(['category_id' => $category->id]);
		
	    $this->assertSame($transaction->category->id, $category->id);
	}

	/**
	 * @test
	 */
	public function it_converts_two_decimal_places_numbers_into_integer_string()
	{
		$transaction = factory(Transaction::class)->create();

		$this->assertSame('44423', array_get($transaction->getAttributes(), 'amount'));
	}

	/**
	 * @test
	 */
	public function it_formats_the_transaction_amount_as_a_float_string()
	{
	    $transaction = factory(Transaction::class)->create();

	    $this->assertSame('444.23', $transaction->amount);
	}

	/**
	 * @test
	 */
	public function it_soft_deletes_the_transaction()
	{
		$transaction = factory(Transaction::class)->create();

		$transaction->delete();

		$this->assertCount(0, Transaction::withoutGlobalScope(AuthedUserScope::class)->get());
		$this->assertCount(1, Transaction::withoutGlobalScope(AuthedUserScope::class)->withTrashed()->get());
	}
}
