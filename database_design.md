## Categories
- ID
- Name
- Timestamps

## Transactions
- ID
- User_id
- Amount
- Date and Time
- Category ID
- Description
- Notes
- Place
- Timestamps
- Soft Deletes

## Users
- ID
- Name
- Balance
- Email
- Password
- City
- Country
- birthdate
